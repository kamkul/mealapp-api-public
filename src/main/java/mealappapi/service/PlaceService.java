package mealappapi.service;

import mealappapi.exception.PlaceNotFoundException;
import mealappapi.model.Place;
import mealappapi.repository.PlaceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("Place")
public class PlaceService {

    private final PlaceRepository repository;

    public PlaceService(PlaceRepository repository) {
        this.repository = repository;
    }

    public List<Place> findAll() {
        return repository.findAll();
    }

    public Place save(Place newPlace) {
        return repository.save(newPlace);
    }

    public Place findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new PlaceNotFoundException(id));
    }

    public Place replacePlace(Place newPlace, Integer id) {
        return repository.findById(id)
                .map(place -> {
                    place.setLatitude(newPlace.getLatitude());
                    place.setLongitude(newPlace.getLongitude());
                    place.setVoivodeship(newPlace.getVoivodeship());
                    place.setName(newPlace.getName());
                    return repository.save(place);
                })
                .orElseGet(() -> {
                    newPlace.setId(id);
                    return repository.save(newPlace);
                });
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }
}
