package mealappapi.service;

import mealappapi.exception.EatingHouseNotFoundException;
import mealappapi.model.Content;
import mealappapi.model.EatingHouse;
import mealappapi.model.EatingHouseTag;
import mealappapi.model.Menu;
import mealappapi.repository.EatingHouseRepository;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("EatingHouse")
public class EatingHouseService {

    private final EatingHouseRepository repository;

    public EatingHouseService(EatingHouseRepository repository) {
        this.repository = repository;
    }

    public List<EatingHouse> findAll() {
        return repository.findAll();
    }

    public EatingHouse save(EatingHouse newEatingHouse) {
        return repository.save(newEatingHouse);
    }

    public EatingHouse findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new EatingHouseNotFoundException(id));
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    public EatingHouse replaceEatingHouse(EatingHouse newEatingHouse, Integer id) {
        return repository.findById(id)
                .map(eatingHouse -> {
                    eatingHouse.setMenuList(newEatingHouse.getMenuList());
                    eatingHouse.setName(newEatingHouse.getName());
                    return repository.save(eatingHouse);
                })
                .orElseGet(() -> {
                    newEatingHouse.setId(id);
                    return repository.save(newEatingHouse);
                });
    }

    public EatingHouse findBySlug(String slug) {
        EatingHouse eatingHouse = repository.findBySlug(slug);

        LocalDateTime start = LocalDateTime.now().with(LocalTime.MIN).minusDays(2);
        LocalDateTime end = LocalDateTime.now().with(LocalTime.MIN).plusDays(2);

        if (eatingHouse != null) {
            eatingHouse.setMenuList(eatingHouse.getMenuList().stream().filter(menu ->
            {
                LocalDateTime menuTime = menu.getDate().toInstant().atZone(ZoneId.of("UTC")).toLocalDateTime();
                return menuTime.isAfter(start) && menuTime.isBefore(end);
            }).collect(Collectors.toList()));

            return eatingHouse;
        } else throw new EatingHouseNotFoundException(slug);
    }

    public List<EatingHouseTag> eatingHouseTagListForEatingHouseName(String slug) {
        EatingHouse eatingHouse = repository.findBySlug(slug);
        if (eatingHouse != null)
            return eatingHouse.getEatingHouseTagList();
        else throw new EatingHouseNotFoundException(slug);
    }

    /*
     * Wydaje mi się, że pobiera całą kolekcję, którą potem filtruje, może to się okazać zabójcze dla bazy danych,
     * kiedy będzie więcej danych do pobrania. Na ten moment zostaje tak, trzeba chyba poprostu napisać zapytanie
     * do bazy danych odpowiednie
     */
    public List<EatingHouse> getEatingHousesWithLimitedData() {
        List<EatingHouse> eatingHouses = repository.findAll();

        LocalDateTime start = LocalDateTime.now().with(LocalTime.MIN).minusDays(2);
        LocalDateTime end = LocalDateTime.now().with(LocalTime.MIN).plusDays(2);

        for (EatingHouse eatingHouse : eatingHouses) {
            eatingHouse.setMenuList(eatingHouse.getMenuList().stream().filter(menu ->
            {
                LocalDateTime menuTime = menu.getDate().toInstant().atZone(ZoneId.of("UTC")).toLocalDateTime();
                return menuTime.isAfter(start) && menuTime.isBefore(end);
            }).collect(Collectors.toList()));
        }

        return eatingHouses;
    }

    /*
     * Wydaje mi się, że pobiera całą kolekcję, którą potem filtruje, może to się okazać zabójcze dla bazy danych,
     * kiedy będzie więcej danych do pobrania. Na ten moment zostaje tak, trzeba chyba poprostu napisać zapytanie
     * do bazy danych odpowiednie
     */
    public List<EatingHouse> getEatingHousesWithLimitedDataForToday() {
        List<EatingHouse> eatingHouses = repository.findAll();

        LocalDateTime now = LocalDateTime.now().with(LocalTime.MIN);

        for (EatingHouse eatingHouse : eatingHouses) {
            if (eatingHouse.getMenuList().size() < 1) {
                List<Content> contentList = new ArrayList<>();
                List<Menu> menuList = new ArrayList<>();
                Content content = new Content();
                Menu menu = new Menu();

                content.setType("DAILY");
                content.setContent("Brak dziennego menu na dziś");
                content.setMenu(menu);
                contentList.add(content);

                menu.setContentList(contentList);
                menu.setDate(Date.from(now.toInstant(ZoneOffset.UTC)));
                menuList.add(menu);
                eatingHouse.setMenuList(menuList);
                continue;
            }
            for (Menu menu : eatingHouse.getMenuList()) {
                LocalDateTime menuTime = menu.getDate().toInstant().atZone(ZoneId.of("UTC")).toLocalDateTime();
                List<Menu> menuList = new ArrayList<>();

                if (menuTime.compareTo(now) == 0) {
                    menu.setContentList(menu.getContentList().stream().sorted().collect(Collectors.toList()));

                    int dailyContentCount = (int) menu.getContentList().stream().filter(content -> content.getType().equals("DAILY")).count();
                    if (dailyContentCount < 1) {
                        List<Content> contentList = menu.getContentList();
                        Content content = new Content();

                        content.setType("DAILY");
                        content.setContent("Brak dziennego menu na dziś");
                        content.setMenu(menu);
                        contentList.add(content);

                        menu.setContentList(contentList);
                        menuList.add(menu);
                        eatingHouse.setMenuList(menuList);
                    } else {
                        menuList.add(menu);
                        eatingHouse.setMenuList(menuList);
                    }
                }
            }

        }


        return eatingHouses;
    }

    public List<EatingHouse> getEatingHousesInPlace(String voivodeshipSlug, String placeSlug) {
        List<EatingHouse> eatingHouses = repository.findByPlace(voivodeshipSlug, placeSlug);

        LocalDate now = LocalDate.now();
        System.out.println(now.toString());

        for (EatingHouse eatingHouse : eatingHouses) {
            Optional<Menu> menuForToday = eatingHouse.getMenuList().stream().filter(
                    lmenu -> lmenu.getDate().toInstant().atZone(ZoneId.of("UTC")).toLocalDate().equals(now)
            ).findFirst();
            if (menuForToday.isPresent()) {
                List<Menu> menuList = new ArrayList<>();
                menuList.add(menuForToday.get());
                eatingHouse.setMenuList(menuList);
            } else {
                List<Content> contentList = new ArrayList<>();
                List<Menu> menuList = new ArrayList<>();
                Content content = new Content();
                Menu menu = new Menu();

                content.setType("DAILY");
                content.setContent("Brak dziennego menu na dziś");
                content.setMenu(menu);
                contentList.add(content);

                menu.setContentList(contentList);
                menu.setDate(Date.from(now.atStartOfDay().toInstant(ZoneOffset.UTC)));
                menuList.add(menu);
                eatingHouse.setMenuList(menuList);
            }
        }

        // To poniżej jest po to, by zlikwidować duplikaty, które tworzą się podczas iteracji.
        // To powinno być inaczej zrobione.
        return new ArrayList<>(new LinkedHashSet<>(eatingHouses));
    }

    public EatingHouse getEatingHouseDetailsInPlace(String eatingHouseSlug, String voivodeshipSlug, String placeSlug) {
        EatingHouse eatingHouse = repository.findEatingHouseByVoivodeshipAndPlace(eatingHouseSlug, voivodeshipSlug, placeSlug);

        LocalDate now = LocalDate.now();
        LocalDate start = now.minusDays(4);
        LocalDate end = now.plusDays(3);

        eatingHouse.setAddressList(eatingHouse.getAddressList().stream().filter(address ->
                address.getPlace().getVoivodeship().getSlug().equals(voivodeshipSlug) &&
                        address.getPlace().getSlug().equals(placeSlug)
        ).collect(Collectors.toList()));

//        eatingHouse.setMenuList(eatingHouse.getMenuList().stream().filter(menu ->
//        {
//            menu.setContentList(menu.getContentList().stream().sorted().collect(Collectors.toList()));
//            LocalDateTime menuTime = menu.getDate().toInstant().atZone(ZoneId.of("UTC")).toLocalDateTime();
//            return menuTime.isAfter(start) && menuTime.isBefore(end);
//        }).sorted().collect(Collectors.toList()));

        List<Menu> menuList = new ArrayList<>();
        LocalDate i = start;
        for (; i.isBefore(end); i = i.plusDays(1)) {
            LocalDate finalI = i;
            Optional<Menu> optionalMenu = eatingHouse.getMenuList().stream().filter(
                    lmenu -> lmenu.getDate().toInstant().atZone(ZoneId.of("UTC")).toLocalDate().equals(finalI)
            ).findFirst();
            if (optionalMenu.isPresent()) {
                Menu menu = optionalMenu.get();
                if (i.isAfter(start) && i.isBefore(now.plusDays(1))) {
                    int dailyContentCount = (int) menu.getContentList().stream().filter(content -> content.getType().equals("DAILY")).count();

                    if (dailyContentCount < 1) {
                        List<Content> contentList = menu.getContentList();
                        Content content = new Content();

                        content.setType("DAILY");
                        content.setContent("Brak dziennego menu na ten dzień");
                        content.setMenu(menu);
                        contentList.add(content);

                        menu.setContentList(contentList);
                        menuList.add(menu);
                    } else {
                        menuList.add(menu);
                    }
                } else if (i.isAfter(start) && i.isBefore(end)) {
                    int dailyContentCount = (int) menu.getContentList().stream().filter(content -> content.getType().equals("DAILY")).count();

                    if (dailyContentCount < 1) {
                        Menu newMenu = new Menu();
                        List<Content> contentList = new ArrayList<>();
                        Content content = new Content();

                        content.setType("DAILY");
                        content.setContent("Brak dziennego menu na ten dzień");
                        content.setMenu(menu);
                        contentList.add(content);

                        menu.setContentList(contentList);
                        menuList.add(newMenu);
                    } else {
                        menuList.add(menu);
                    }
                }
            } else {
                if (i.isAfter(start) && i.isBefore(now.plusDays(1))) {
                    List<Content> contentList = new ArrayList<>();
                    Content content = new Content();
                    Menu menu = new Menu();

                    content.setType("DAILY");
                    content.setContent("Brak dziennego menu na ten dzień");
                    content.setMenu(menu);
                    contentList.add(content);

                    menu.setContentList(contentList);
                    menu.setDate(Date.from(i.atStartOfDay().toInstant(ZoneOffset.UTC)));
                    menuList.add(menu);
                } else if (i.isAfter(start) && i.isBefore(end)) {
                    eatingHouse.setMenuList(menuList);
                    return eatingHouse;
                }
            }
        }

        eatingHouse.setMenuList(menuList);
        return eatingHouse;
    }
}
