package mealappapi.service;

import mealappapi.exception.OpenHoursNotFoundException;
import mealappapi.model.OpenHours;
import mealappapi.repository.OpenHoursRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("OpenHours")
public class OpenHoursService {

    private final OpenHoursRepository repository;

    public OpenHoursService(OpenHoursRepository repository) {
        this.repository = repository;
    }

    public List<OpenHours> findAll() {
        return repository.findAll();
    }

    public OpenHours save(OpenHours newOpenHours) {
        return repository.save(newOpenHours);
    }

    public OpenHours findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new OpenHoursNotFoundException(id));
    }

    public OpenHours replaceOpenHours(OpenHours newOpenHours, Integer id) {
        return repository.findById(id)
                .map(openHours -> {
                    openHours.setDay(newOpenHours.getDay());
                    openHours.setAddress(newOpenHours.getAddress());
                    openHours.setFromHour(newOpenHours.getFromHour());
                    openHours.setToHour(newOpenHours.getToHour());
                    return repository.save(newOpenHours);
                })
                .orElseGet(() -> {
                    newOpenHours.setId(id);
                    return repository.save(newOpenHours);
                });
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }
}
