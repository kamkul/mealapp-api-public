package mealappapi.service;

import mealappapi.exception.EatingHouseNotFoundException;
import mealappapi.model.EatingHouseTag;
import mealappapi.repository.EatingHouseTagRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("EatingHouseTag")
public class EatingHouseTagService {

    private final EatingHouseTagRepository repository;

    public EatingHouseTagService(EatingHouseTagRepository repository) {
        this.repository = repository;
    }

    public List<EatingHouseTag> findAll() {
        return repository.findAll();
    }

    public EatingHouseTag save(EatingHouseTag newEatingHouseTag) {
        return repository.save(newEatingHouseTag);
    }

    public EatingHouseTag findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new EatingHouseNotFoundException(id));
    }

    public EatingHouseTag replaceEatingHouseTag(EatingHouseTag newEatingHouseTag, Integer id) {
        return repository.findById(id)
                .map(eatingHouseTag -> {
                    eatingHouseTag.setEatingHouseList(newEatingHouseTag.getEatingHouseList());
                    eatingHouseTag.setName(newEatingHouseTag.getName());
                    return repository.save(eatingHouseTag);
                })
                .orElseGet(() -> {
                    newEatingHouseTag.setId(id);
                    return repository.save(newEatingHouseTag);
                });
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }
}
