package mealappapi.service;

import mealappapi.exception.AddressNotFoundException;
import mealappapi.model.Address;
import mealappapi.repository.AddressRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("Address")
public class AddressService {

    private final AddressRepository repository;

    public AddressService(AddressRepository repository) {
        this.repository = repository;
    }

    public List<Address> findAll() {
        return repository.findAll();
    }

    public Address save(Address newAddress) {
        return repository.save(newAddress);
    }

    public Address findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new AddressNotFoundException(id));
    }

    public Address replaceAddress(Address newAddress, Integer id) {
        return repository.findById(id)
                .map(address -> {
                    address.setAddress(newAddress.getAddress());
                    address.setCity(newAddress.getCity());
                    address.setCountry(newAddress.getCountry());
                    address.setEatingHouse(newAddress.getEatingHouse());
                    address.setGoogleMapsUrl(newAddress.getGoogleMapsUrl());
                    address.setLatitude(newAddress.getLatitude());
                    address.setLongitude(newAddress.getLongitude());
                    address.setPhoneNumber(newAddress.getPhoneNumber());
                    return repository.save(address);
                })
                .orElseGet(() -> {
                    newAddress.setId(id);
                    return repository.save(newAddress);
                });
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }
}
