package mealappapi.service;

import mealappapi.exception.FavouriteMealNotFoundException;
import mealappapi.model.FavouriteMeal;
import mealappapi.repository.FavouriteMealRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("FavouriteMeal")
public class FavouriteMealService {

    private final FavouriteMealRepository repository;

    public FavouriteMealService(FavouriteMealRepository repository) {
        this.repository = repository;
    }

    public List<FavouriteMeal> findAll() {
        return repository.findAll();
    }

    public FavouriteMeal save(FavouriteMeal newFavouriteMeal) {
        return repository.save(newFavouriteMeal);
    }

    public FavouriteMeal findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new FavouriteMealNotFoundException(id));
    }

    public FavouriteMeal replaceFavouriteMeal(FavouriteMeal newFavouriteMeal, Integer id) {
        return repository.findById(id)
                .map(favouriteMeal -> {
                    favouriteMeal.setUser(newFavouriteMeal.getUser());
                    favouriteMeal.setName(newFavouriteMeal.getName());
                    return repository.save(favouriteMeal);
                })
                .orElseGet(() -> {
                    newFavouriteMeal.setId(id);
                    return repository.save(newFavouriteMeal);
                });
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }
}
