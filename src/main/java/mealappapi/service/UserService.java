package mealappapi.service;

import mealappapi.exception.UserNotFoundException;
import mealappapi.model.EatingHouse;
import mealappapi.model.FavouriteMeal;
import mealappapi.model.User;
import mealappapi.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("User")
public class UserService {

    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public List<User> findAll() {
        return repository.findAll();
    }

    public User save(User newUser) {
        return repository.save(newUser);
    }

    public User findByNickname(String nickname) {
        User user = repository.findByNickname(nickname);
        if (user != null)
            return user;
        else throw new UserNotFoundException(nickname);
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    public User replaceUser(User newUser, Integer id) {
        return repository.findById(id)
                .map(user -> {
                    user.setEmail(newUser.getEmail());
                    user.setFavouriteMealList(newUser.getFavouriteMealList());
                    user.setNickname(newUser.getNickname());
                    user.setPassword(newUser.getPassword());
                    return repository.save(newUser);
                })
                .orElseGet(() -> {
                    newUser.setId(id);
                    return repository.save(newUser);
                });
    }

    public List<FavouriteMeal> getFavouriteMealsByUserNickname(String nickname) {
        User user = repository.findByNickname(nickname);
        if (user != null) {
            return user.getFavouriteMealList();
        } else throw new UserNotFoundException(nickname);
    }

    public List<EatingHouse> getFavouriteEatingHousesByUserNickname(String nickname) {
        User user = repository.findByNickname(nickname);
        if (user != null) {
            return user.getFavouriteEatingHouses();
        } else throw new UserNotFoundException(nickname);
    }
}
