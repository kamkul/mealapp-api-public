package mealappapi.service;

import mealappapi.exception.VoivodeshipNotFoundException;
import mealappapi.model.Place;
import mealappapi.model.Voivodeship;
import mealappapi.repository.VoivodeshipRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service("Voivodeship")
public class VoivodeshipService {

    private final VoivodeshipRepository repository;

    public VoivodeshipService(VoivodeshipRepository repository) {
        this.repository = repository;
    }

    public List<Voivodeship> findAll() {
        return repository.findAll();
    }

    public Voivodeship save(Voivodeship newVoivodeship) {
        return repository.save(newVoivodeship);
    }

    public Voivodeship findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new VoivodeshipNotFoundException(id));
    }

    public Voivodeship replaceVoivodeship(Voivodeship newVoivodeship, Integer id) {
        return repository.findById(id)
                .map(voivodeship -> {
                    voivodeship.setPlaceList(newVoivodeship.getPlaceList());
                    voivodeship.setName(newVoivodeship.getName());
                    return repository.save(voivodeship);
                })
                .orElseGet(() -> {
                    newVoivodeship.setId(id);
                    return repository.save(newVoivodeship);
                });
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    public List<Place> getPlacesInVoivodeship(String slug) {
        Voivodeship voivodeship = repository.findBySlug(slug);
        if (voivodeship != null) {
            return voivodeship.getPlaceList();
        } else throw new VoivodeshipNotFoundException(slug);
    }

    public Place getPlaceDetailInVoivodeship(String slug, String placeSlug) {
        Voivodeship voivodeship = repository.findBySlug(slug);

        return voivodeship.getPlaceList().stream().filter(place ->
                place.getSlug().equals(placeSlug)).findFirst().orElse(null);
    }
}
