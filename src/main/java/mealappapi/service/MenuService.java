package mealappapi.service;

import mealappapi.exception.MenuNotFoundException;
import mealappapi.model.Menu;
import mealappapi.repository.MenuRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("Menu")
public class MenuService {

    private final MenuRepository repository;

    public MenuService(MenuRepository repository) {
        this.repository = repository;
    }

    public List<Menu> findAll() {
        return repository.findAll();
    }

    public Menu save(Menu newMenu) {
        return repository.save(newMenu);
    }

    public Menu findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new MenuNotFoundException(id));
    }

    public Menu replaceMenu(Menu newMenu, Integer id) {
        return repository.findById(id)
                .map(menu -> {
                    menu.setContentList(newMenu.getContentList());
                    menu.setEatingHouse(newMenu.getEatingHouse());
                    menu.setDate(newMenu.getDate());
                    return repository.save(menu);
                })
                .orElseGet(() -> {
                    newMenu.setId(id);
                    return repository.save(newMenu);
                });
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

}
