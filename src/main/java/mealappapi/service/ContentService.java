package mealappapi.service;

import mealappapi.exception.ContentNotFoundException;
import mealappapi.model.Content;
import mealappapi.repository.ContentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("Content")
public class ContentService {

    private final ContentRepository repository;

    public ContentService(ContentRepository repository) {
        this.repository = repository;
    }

    public List<Content> findAll() {
        return repository.findAll();
    }

    public Content save(Content newContent) {
        return repository.save(newContent);
    }

    public Content findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new ContentNotFoundException(id));
    }

    public Content replaceContent(Content newContent, Integer id) {
        return repository.findById(id)
                .map(Content -> {
                    Content.setContent(newContent.getContent());
                    Content.setMenu(newContent.getMenu());
                    Content.setType(newContent.getType());
                    return repository.save(Content);
                })
                .orElseGet(() -> {
                    newContent.setId(id);
                    return repository.save(newContent);
                });
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }
}
