package mealappapi.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHandler {
    private static final DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

    private DateHandler() {
    }

    public static Date parseStringToDate(String date) {
        try {
            return df.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String parseDateToString(Date date) {
        return df.format(date);
    }

}
