package mealappapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonView;
import mealappapi.jsonViews.EatingHouseJsonView;

import javax.persistence.*;

@Entity
public class Content implements Comparable<Content> {

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })
    private String type;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idMenu")
    private Menu menu;

    public Content() {
    }

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })

    private String content;

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Content{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", menu=" + menu +
                ", content='" + content + '\'' +
                '}';
    }

    @Override
    public int compareTo(Content content) {
        if (this.getType().equals(content.getType())) {
            return 0;
        } else if (this.getType().equals("STATIC")) {
            return -1;
        }
        return 1;
    }
}
