package mealappapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import mealappapi.jsonViews.EatingHouseJsonView;

import javax.persistence.*;
import java.util.List;

@Entity
public class Address {
    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.mapsWithEatingHouses.class,
            EatingHouseJsonView.showDetails.class,
    })
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idEatingHouse")
    private EatingHouse eatingHouse;

    @JsonIgnore
    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idPlace")
    private Place place;

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })
    @OneToMany(cascade = {CascadeType.ALL, CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, mappedBy = "address")
    private List<OpenHours> openHoursList;

    @JsonView({
            EatingHouseJsonView.mapsWithEatingHouses.class,
            EatingHouseJsonView.showDetails.class,
    })
    private String city;
    private String country;

    @JsonView({
            EatingHouseJsonView.mapsWithEatingHouses.class,
            EatingHouseJsonView.showDetails.class,
    })
    private String address;

    @JsonView({
            EatingHouseJsonView.mapsWithEatingHouses.class,
            EatingHouseJsonView.showDetails.class,
    })
    private double latitude;

    @JsonView({
            EatingHouseJsonView.mapsWithEatingHouses.class,
            EatingHouseJsonView.showDetails.class,
    })
    private double longitude;

    private String googleMapsUrl;

    @JsonView({
            EatingHouseJsonView.mapsWithEatingHouses.class,
            EatingHouseJsonView.showDetails.class,
    })
    private String phoneNumber;

    public Address() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EatingHouse getEatingHouse() {
        return eatingHouse;
    }

    public void setEatingHouse(EatingHouse eatingHouse) {
        this.eatingHouse = eatingHouse;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGoogleMapsUrl() {
        return googleMapsUrl;
    }

    public void setGoogleMapsUrl(String googleMapsUrl) {
        this.googleMapsUrl = googleMapsUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public List<OpenHours> getOpenHoursList() {
        return openHoursList;
    }

    public void setOpenHoursList(List<OpenHours> openHoursList) {
        this.openHoursList = openHoursList;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
