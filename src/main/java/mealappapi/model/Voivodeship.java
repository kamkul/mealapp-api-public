package mealappapi.model;

import com.fasterxml.jackson.annotation.JsonView;
import mealappapi.jsonViews.VoivodeshipJsonView;

import javax.persistence.*;
import java.util.List;

@Entity
public class Voivodeship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonView({VoivodeshipJsonView.showSlugs.class})
    private String name;

    @JsonView({VoivodeshipJsonView.showSlugs.class})
    private String slug;

    @OneToMany(cascade = {CascadeType.ALL, CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, mappedBy = "voivodeship")
    private List<Place> placeList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Place> getPlaceList() {
        return placeList;
    }

    public void setPlaceList(List<Place> placeList) {
        this.placeList = placeList;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
