package mealappapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonView;
import mealappapi.jsonViews.EatingHouseJsonView;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class EatingHouseTag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonBackReference
    @ManyToMany(mappedBy = "eatingHouseTagList")
    private List<EatingHouse> eatingHouseList = new ArrayList<>();

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })
    private String name;

    public EatingHouseTag() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<EatingHouse> getEatingHouseList() {
        return eatingHouseList;
    }

    public void setEatingHouseList(List<EatingHouse> eatingHouseList) {
        this.eatingHouseList = eatingHouseList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
