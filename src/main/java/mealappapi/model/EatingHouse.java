package mealappapi.model;

import com.fasterxml.jackson.annotation.JsonView;
import mealappapi.jsonViews.EatingHouseJsonView;
import mealappapi.jsonViews.VoivodeshipJsonView;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class EatingHouse {

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
            EatingHouseJsonView.mapsWithEatingHouses.class,
            EatingHouseJsonView.search.class,
    })
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
            EatingHouseJsonView.mapsWithEatingHouses.class,
            EatingHouseJsonView.search.class,
    })
    private String name;

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })
    @OneToMany(cascade = {CascadeType.ALL, CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, mappedBy = "eatingHouse")
    private List<Menu> menuList;

    @JsonView({
            EatingHouseJsonView.showDetails.class,
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.mapsWithEatingHouses.class,
            EatingHouseJsonView.search.class,
    })
    private String slug;

    @JsonView({
            EatingHouseJsonView.showDetails.class,
            EatingHouseJsonView.mapsWithEatingHouses.class,
    })
    @OneToMany(cascade = {CascadeType.ALL, CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, mappedBy = "eatingHouse")
    private List<Address> addressList;

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "EatingHouseHasEatingHouseTag",
            joinColumns = {@JoinColumn(name = "idEatingHouse")},
            inverseJoinColumns = {@JoinColumn(name = "idEatingHouseTag")}
    )
    private List<EatingHouseTag> eatingHouseTagList = new ArrayList<>();

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class
    })
    private String logoUrl;

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })
    private String description;

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })
    private String primaryColor;

    @JsonView({
            EatingHouseJsonView.showDetails.class
    })
    private String photoUrl;

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<EatingHouseTag> getEatingHouseTagList() {
        return eatingHouseTagList;
    }

    public void setEatingHouseTagList(List<EatingHouseTag> eatingHouseTagList) {
        this.eatingHouseTagList = eatingHouseTagList;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public EatingHouse() {
    }

    public void setMenuList(List<Menu> menuList) {

        for (Menu menu : menuList) {
            menu.setEatingHouse(this);
        }

        this.menuList = menuList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "EatingHouse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }
}
