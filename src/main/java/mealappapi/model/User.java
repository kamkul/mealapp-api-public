package mealappapi.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(cascade = {CascadeType.ALL, CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, mappedBy = "user")
    private List<FavouriteMeal> favouriteMealList;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "FavouriteEatingHouse",
            joinColumns = { @JoinColumn(name = "idUser") },
            inverseJoinColumns = { @JoinColumn(name = "idEatingHouse") }
    )
    private List<EatingHouse> favouriteEatingHouses = new ArrayList<>();

    private String nickname;
    private String password;
    private String email;

    public User() {
    }

    public List<EatingHouse> getFavouriteEatingHouses() {
        return favouriteEatingHouses;
    }

    public void setFavouriteEatingHouses(List<EatingHouse> favouriteEatingHouses) {
        this.favouriteEatingHouses = favouriteEatingHouses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<FavouriteMeal> getFavouriteMealList() {
        return favouriteMealList;
    }

    public void setFavouriteMealList(List<FavouriteMeal> favouriteMealList) {
        this.favouriteMealList = favouriteMealList;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
