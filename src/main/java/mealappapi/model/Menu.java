package mealappapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonView;
import mealappapi.helper.DateHandler;
import mealappapi.jsonViews.EatingHouseJsonView;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Menu implements Comparable<Menu> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })
    private int id;

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })
    private Date date;

    @JsonView({
            EatingHouseJsonView.simplified.class,
            EatingHouseJsonView.showDetails.class,
    })
    @OneToMany(cascade = {CascadeType.ALL, CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, mappedBy = "menu")
    private List<Content> contentList;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idEatingHouse")
    private EatingHouse eatingHouse;

    public Menu() {
    }

    public List<Content> getContentList() {
        return contentList;
    }

    public void setContentList(List<Content> contentList) {

        for (Content content : contentList) {
            content.setMenu(this);
        }

        this.contentList = contentList;
    }

    public EatingHouse getEatingHouse() {
        return eatingHouse;
    }

    public void setEatingHouse(EatingHouse eatingHouse) {
        this.eatingHouse = eatingHouse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Menu menu = (Menu) o;

        String thisDate = DateHandler.parseDateToString(date);
        String comparedDate = DateHandler.parseDateToString(((Menu) o).date);

        return date != null ? thisDate.equals(comparedDate) : menu.date == null;
    }

    @Override
    public int compareTo(Menu menu) {
        if (this.getDate().before(menu.getDate()))
            return -1;
        else if (this.getDate().after(menu.getDate()))
            return 1;
        return 0;
    }
}
