package mealappapi.controller;

import mealappapi.exception.ContentNotFoundException;
import mealappapi.model.Content;
import mealappapi.repository.ContentRepository;
import mealappapi.service.ContentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ContentController {
    private final ContentService service;

    public ContentController(ContentService repository) {
        this.service = repository;
    }

    @GetMapping("/contents")
    List<Content> getAll() {
        return service.findAll();
    }

    @PostMapping("/contents")
    Content postNewContent(@RequestBody Content newContent) {
        return service.save(newContent);
    }

    @GetMapping("/contents/{id}")
    Content getContent(@PathVariable Integer id) {
        return service.findById(id);
    }

    @PutMapping("/contents/{id}")
    Content replaceContent(@RequestBody Content newContent, @PathVariable Integer id) {
        return service.replaceContent(newContent, id);
    }

    @DeleteMapping("/contents/{id}")
    void deleteContent(@PathVariable Integer id) {
        service.deleteById(id);
    }
}
