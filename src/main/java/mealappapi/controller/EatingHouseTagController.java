package mealappapi.controller;

import mealappapi.model.EatingHouseTag;
import mealappapi.service.EatingHouseTagService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EatingHouseTagController {
    private final EatingHouseTagService service;

    public EatingHouseTagController(EatingHouseTagService service) {
        this.service = service;
    }

    @GetMapping("/eatingHouseTags")
    List<EatingHouseTag> getAll() {
        return service.findAll();
    }

    @PostMapping("/eatingHouseTags")
    EatingHouseTag postNewEatingHouseTag(@RequestBody EatingHouseTag newEatingHouseTag) {
        return service.save(newEatingHouseTag);
    }

    @GetMapping("/eatingHouseTags/{id}")
    EatingHouseTag getEatingHouseTag(@PathVariable Integer id) {
        return service.findById(id);
    }

    @PutMapping("/eatingHouseTags/{id}")
    EatingHouseTag replaceEatingHouseTag(@RequestBody EatingHouseTag newEatingHouseTag, @PathVariable Integer id) {
        return service.replaceEatingHouseTag(newEatingHouseTag, id);
    }

    @DeleteMapping("/eatingHouseTags/{id}")
    void deleteEatingHouseTag(@PathVariable Integer id) {
        service.deleteById(id);
    }
}
