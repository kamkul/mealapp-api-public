package mealappapi.controller;

import mealappapi.exception.MenuNotFoundException;
import mealappapi.model.Menu;
import mealappapi.repository.MenuRepository;
import mealappapi.service.MenuService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MenuController {
    private final MenuService service;

    public MenuController(MenuService service) {
        this.service = service;
    }

    @GetMapping("/menus")
    List<Menu> getAll() {
        return service.findAll();
    }

    @PostMapping("/menus")
    Menu postNewMenu(@RequestBody Menu newMenu) {
        return service.save(newMenu);
    }

    @GetMapping("/menus/{id}")
    Menu getMenu(@PathVariable Integer id) {
        return service.findById(id);
    }

    @PutMapping("/menus/{id}")
    Menu replaceMenu(@RequestBody Menu newMenu, @PathVariable Integer id) {
        return service.replaceMenu(newMenu, id);
    }

    @DeleteMapping("/menus/{id}")
    void deleteMenu(@PathVariable Integer id) {
        service.deleteById(id);
    }
}
