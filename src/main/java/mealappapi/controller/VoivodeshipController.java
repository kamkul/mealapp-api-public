package mealappapi.controller;

import com.fasterxml.jackson.annotation.JsonView;
import mealappapi.jsonViews.VoivodeshipJsonView;
import mealappapi.model.Place;
import mealappapi.model.Voivodeship;
import mealappapi.service.VoivodeshipService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class VoivodeshipController {
    private final VoivodeshipService service;

    public VoivodeshipController(VoivodeshipService service) {
        this.service = service;
    }

    @GetMapping("/voivodeships")
    List<Voivodeship> getAll() {
        return service.findAll();
    }

    @PostMapping("/voivodeships")
    Voivodeship postNewVoivodeship(@RequestBody Voivodeship newVoivodeship) {
        return service.save(newVoivodeship);
    }

    @GetMapping("/voivodeships/{id}")
    Voivodeship getVoivodeship(@PathVariable Integer id) {
        return service.findById(id);
    }

    @PutMapping("/voivodeships/{id}")
    Voivodeship replaceVoivodeship(@RequestBody Voivodeship newVoivodeship, @PathVariable Integer id) {
        return service.replaceVoivodeship(newVoivodeship, id);
    }

    @DeleteMapping("/voivodeships/{id}")
    void deleteVoivodeship(@PathVariable Integer id) {
        service.deleteById(id);
    }

    @GetMapping("/voivodeships/{slug}/places")
    List<Place> getPlacesInVoivodeship(@PathVariable String slug) {
        return service.getPlacesInVoivodeship(slug);
    }

    @GetMapping("/voivodeships/{slug}/places/{placeSlug}")
    Place getPlaceDetailInVoivodeship(@PathVariable String slug, @PathVariable String placeSlug) {
        return service.getPlaceDetailInVoivodeship(slug, placeSlug);
    }

    @JsonView(VoivodeshipJsonView.showSlugs.class)
    @GetMapping("/voivodeships/slugs")
    List<Voivodeship> getVoivodeshipsSlugs() {
        return service.findAll();
    }
}
