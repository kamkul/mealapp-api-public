package mealappapi.controller;

import mealappapi.model.EatingHouse;
import mealappapi.model.FavouriteMeal;
import mealappapi.model.User;
import mealappapi.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("/user")
    List<User> getAll() {
        return service.findAll();
    }

    @PostMapping("/user")
    User postNewUser(@RequestBody User newUser) {
        return service.save(newUser);
    }

    @GetMapping("/user/{nickname}")
    User getUser(@PathVariable String nickname) {
        return service.findByNickname(nickname);
    }

    @GetMapping("/user/{nickname}/eatingHouses")
    List<EatingHouse> getUserFavouriteEatingHouses(@PathVariable String nickname) {
        return service.getFavouriteEatingHousesByUserNickname(nickname);
    }

    @GetMapping("/user/{nickname}/meals")
    List<FavouriteMeal> getUserFavouriteMeals(@PathVariable String nickname) {
        return service.getFavouriteMealsByUserNickname(nickname);
    }

    @PutMapping("/user/{id}")
    User replaceUser(@RequestBody User newUser, @PathVariable Integer id) {
        return service.replaceUser(newUser, id);
    }

    @DeleteMapping("/user/{id}")
    void deleteUser(@PathVariable Integer id) {
        service.deleteById(id);
    }
}
