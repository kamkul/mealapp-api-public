package mealappapi.controller;

import mealappapi.exception.OpenHoursNotFoundException;
import mealappapi.model.OpenHours;
import mealappapi.repository.OpenHoursRepository;
import mealappapi.service.OpenHoursService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OpenHoursController {
    private final OpenHoursService service;

    public OpenHoursController(OpenHoursService service) {
        this.service = service;
    }

    @GetMapping("/openHours")
    List<OpenHours> getAll() {
        return service.findAll();
    }

    @PostMapping("/openHours")
    OpenHours postNewOpenHours(@RequestBody OpenHours newOpenHours) {
        return service.save(newOpenHours);
    }

    @GetMapping("/openHours/{id}")
    OpenHours getOpenHours(@PathVariable Integer id) {
        return service.findById(id);
    }

    @PutMapping("/openHours/{id}")
    OpenHours replaceOpenHours(@RequestBody OpenHours newOpenHours, @PathVariable Integer id) {
        return service.replaceOpenHours(newOpenHours, id);
    }

    @DeleteMapping("/openHours/{id}")
    void deleteOpenHours(@PathVariable Integer id) {
        service.deleteById(id);
    }
}
