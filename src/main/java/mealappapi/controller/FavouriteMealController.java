package mealappapi.controller;

import mealappapi.exception.FavouriteMealNotFoundException;
import mealappapi.model.FavouriteMeal;
import mealappapi.repository.FavouriteMealRepository;
import mealappapi.service.FavouriteMealService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FavouriteMealController {
    private final FavouriteMealService service;

    public FavouriteMealController(FavouriteMealService service) {
        this.service = service;
    }

    @GetMapping("/favouriteMeals")
    List<FavouriteMeal> getAll() {
        return service.findAll();
    }

    @PostMapping("/favouriteMeals")
    FavouriteMeal postNewFavouriteMeal(@RequestBody FavouriteMeal newFavouriteMeal) {
        return service.save(newFavouriteMeal);
    }

    @GetMapping("/favouriteMeals/{id}")
    FavouriteMeal getFavouriteMeal(@PathVariable Integer id) {
        return service.findById(id);
    }

    @PutMapping("/favouriteMeals/{id}")
    FavouriteMeal replaceFavouriteMeal(@RequestBody FavouriteMeal newFavouriteMeal, @PathVariable Integer id) {
        return service.replaceFavouriteMeal(newFavouriteMeal, id);
    }

    @DeleteMapping("/favouriteMeals/{id}")
    void deleteFavouriteMeal(@PathVariable Integer id) {
        service.deleteById(id);
    }
}
