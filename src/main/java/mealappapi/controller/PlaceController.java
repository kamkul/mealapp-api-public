package mealappapi.controller;

import com.fasterxml.jackson.annotation.JsonView;
import mealappapi.jsonViews.PlaceJsonView;
import mealappapi.jsonViews.VoivodeshipJsonView;
import mealappapi.model.Place;
import mealappapi.service.PlaceService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PlaceController {
    private final PlaceService service;

    public PlaceController(PlaceService service) {
        this.service = service;
    }

    @GetMapping("/places")
    List<Place> getAll() {
        return service.findAll();
    }

    @PostMapping("/places")
    Place postNewPlace(@RequestBody Place newPlace) {
        return service.save(newPlace);
    }

    @GetMapping("/places/{id}")
    Place getPlace(@PathVariable Integer id) {
        return service.findById(id);
    }

    @PutMapping("/places/{id}")
    Place replacePlace(@RequestBody Place newPlace, @PathVariable Integer id) {
        return service.replacePlace(newPlace, id);
    }

    @DeleteMapping("/places/{id}")
    void deletePlace(@PathVariable Integer id) {
        service.deleteById(id);
    }

    @JsonView(PlaceJsonView.showSlugs.class)
    @GetMapping("/places/slugs")
    List<Place> getPlacesSlugs() {
        return service.findAll();
    }
}
