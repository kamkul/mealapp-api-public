package mealappapi.controller;

import mealappapi.exception.AddressNotFoundException;
import mealappapi.model.Address;
import mealappapi.service.AddressService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AddressController {
    private final AddressService service;

    public AddressController(AddressService service) {
        this.service = service;
    }

    @GetMapping("/adresses")
    List<Address> getAll() {
        return service.findAll();
    }

    @PostMapping("/adresses")
    Address postNewAddress(@RequestBody Address newAddress) {
        return service.save(newAddress);
    }

    @GetMapping("/addresses/{id}")
    Address getAddress(@PathVariable Integer id) {
        return service.findById(id);
    }

    @PutMapping("/addresses/{id}")
    Address replaceAddress(@RequestBody Address newAddress, @PathVariable Integer id) {
        return service.replaceAddress(newAddress, id);
    }

    @DeleteMapping("/addresses/{id}")
    void deleteEmployee(@PathVariable Integer id) {
        service.deleteById(id);
    }
}
