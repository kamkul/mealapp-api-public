package mealappapi.controller;

import com.fasterxml.jackson.annotation.JsonView;
import mealappapi.jsonViews.EatingHouseJsonView;
import mealappapi.model.EatingHouse;
import mealappapi.model.EatingHouseTag;
import mealappapi.service.EatingHouseService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EatingHouseController {
    private final EatingHouseService service;

    public EatingHouseController(EatingHouseService service) {
        this.service = service;
    }

    @JsonView(EatingHouseJsonView.simplified.class)
    @GetMapping("/eatingHouses")
    List<EatingHouse> getAll() {
        return service.getEatingHousesWithLimitedDataForToday();
    }

    @JsonView(EatingHouseJsonView.search.class)
    @GetMapping("/eatingHousesNames")
    List<EatingHouse> getNamesAndSlugs() {
        return service.findAll();
    }


//    @GetMapping("/eatingHousesLimitedDataTest")
//    List<EatingHouse> getEatingHousesWithLimitedData() {
//        return service.getEatingHousesWithLimitedData();
//    }

    @GetMapping("/eatingHousesLimitedDataTestForToday")
    List<EatingHouse> getEatingHousesWithLimitedDataForToday() {
        return service.getEatingHousesWithLimitedDataForToday();
    }

    @PostMapping("/eatingHouses")
    EatingHouse postNewEatingHouse(@RequestBody EatingHouse newEatingHouse) {
        return service.save(newEatingHouse);
    }

    @JsonView(EatingHouseJsonView.showDetails.class)
    @GetMapping("/eatingHouses/{eatingHouseSlug}")
    EatingHouse getEatingHouse(@PathVariable String eatingHouseSlug) {
        return service.findBySlug(eatingHouseSlug);
    }

    @PutMapping("/eatingHouses/{id}")
    EatingHouse replaceEatingHouse(@RequestBody EatingHouse newEatingHouse, @PathVariable Integer id) {
        return service.replaceEatingHouse(newEatingHouse, id);
    }

    @DeleteMapping("/eatingHouses/{id}")
    void deleteEatingHouse(@PathVariable Integer id) {
        service.deleteById(id);
    }

    @GetMapping("/eatingHouses/{name}/tags")
    List<EatingHouseTag> getEatingHouseTagList(@PathVariable String name) {
        return service.eatingHouseTagListForEatingHouseName(name);
    }

    @JsonView(EatingHouseJsonView.simplified.class)
    @GetMapping("/eatingHouses/voivodeship/{voivodeshipSlug}/place/{placeSlug}")
    List<EatingHouse> getEatingHouseInPlace(@PathVariable String voivodeshipSlug, @PathVariable String placeSlug) {
        return service.getEatingHousesInPlace(voivodeshipSlug, placeSlug);
    }

    @JsonView(EatingHouseJsonView.mapsWithEatingHouses.class)
    @GetMapping("/map/eatingHouses/voivodeship/{slug}/place/{placeSlug}")
    List<EatingHouse> getEatingHouseInPlaceForMap(@PathVariable String slug, @PathVariable String placeSlug) {
        return service.getEatingHousesInPlace(slug, placeSlug);
    }

    @JsonView(EatingHouseJsonView.showDetails.class)
    @GetMapping("/eatingHouses/{eatingHouseSlug}/voivodeship/{voivodeshipSlug}/place/{placeSlug}")
    EatingHouse getEatingHouseDetailsInPlace(
            @PathVariable String eatingHouseSlug,
            @PathVariable String voivodeshipSlug,
            @PathVariable String placeSlug
    ) {
        return service.getEatingHouseDetailsInPlace(eatingHouseSlug, voivodeshipSlug, placeSlug);
    }
}
