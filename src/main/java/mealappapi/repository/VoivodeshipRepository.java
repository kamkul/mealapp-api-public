package mealappapi.repository;

import mealappapi.model.Voivodeship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface VoivodeshipRepository extends JpaRepository<Voivodeship, Integer> {
    @Query(value = "SELECT * FROM Voivodeship WHERE slug = ?", nativeQuery = true)
    Voivodeship findBySlug(String slug);
}