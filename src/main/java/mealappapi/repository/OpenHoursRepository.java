package mealappapi.repository;

import mealappapi.model.Menu;
import mealappapi.model.OpenHours;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OpenHoursRepository extends JpaRepository<OpenHours, Integer> {

}