package mealappapi.repository;

import mealappapi.model.FavouriteMeal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FavouriteMealRepository extends JpaRepository<FavouriteMeal, Integer> {

}