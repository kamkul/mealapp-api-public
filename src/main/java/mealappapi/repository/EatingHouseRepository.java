package mealappapi.repository;

import mealappapi.model.EatingHouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EatingHouseRepository extends JpaRepository<EatingHouse, Integer> {
    @Query(value = "SELECT * FROM EatingHouse WHERE slug = ?", nativeQuery = true)
    EatingHouse findBySlug(String nickname);

    @Query(value = "SELECT DISTINCT * FROM EatingHouse " +
            "INNER JOIN Address " +
            "ON Address.idEatingHouse = EatingHouse.id " +
            "INNER JOIN Place " +
            "ON Address.idPlace = Place.id " +
            "INNER JOIN Voivodeship " +
            "ON Place.idVoivodeship = Voivodeship.id " +
            "WHERE Voivodeship.slug = ? AND Place.slug = ?", nativeQuery = true)
    List<EatingHouse> findByPlace(String voivodeshipslug, String placeSlug);

    @Query(value = "SELECT * FROM EatingHouse " +
            "INNER JOIN Address " +
            "ON Address.idEatingHouse = EatingHouse.id " +
            "INNER JOIN Place " +
            "ON Address.idPlace = Place.id " +
            "INNER JOIN Voivodeship " +
            "ON Place.idVoivodeship = Voivodeship.id " +
            "WHERE EatingHouse.slug = ? AND Voivodeship.slug = ? AND Place.slug = ? ", nativeQuery = true)
    EatingHouse findEatingHouseByVoivodeshipAndPlace(String eatingHouseSlug, String voivodeshipSlug, String placeSlug);

}