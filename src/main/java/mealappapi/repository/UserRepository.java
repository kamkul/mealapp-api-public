package mealappapi.repository;

import mealappapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "SELECT * FROM User WHERE nickname = ?", nativeQuery = true)
    User findByNickname(String nickname);
}