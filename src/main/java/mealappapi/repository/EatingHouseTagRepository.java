package mealappapi.repository;

import mealappapi.model.EatingHouseTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EatingHouseTagRepository extends JpaRepository<EatingHouseTag, Integer> {

}