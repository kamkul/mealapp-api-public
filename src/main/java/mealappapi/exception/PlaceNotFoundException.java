package mealappapi.exception;

public class PlaceNotFoundException extends RuntimeException {

    public PlaceNotFoundException(Integer id) {
        super("Could not find Place - " + id);
    }
}