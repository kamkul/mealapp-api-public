package mealappapi.exception;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String nickname) {
        super("Could not find User - " + nickname);
    }

    public UserNotFoundException(Integer id) {
        super("Could not find User - " + id);
    }
}