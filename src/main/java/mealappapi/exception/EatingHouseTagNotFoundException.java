package mealappapi.exception;

public class EatingHouseTagNotFoundException extends RuntimeException {

    public EatingHouseTagNotFoundException(Integer id) {
        super("Could not find EatingHouseTag - " + id);
    }
}