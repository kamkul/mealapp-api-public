package mealappapi.exception;

public class MenuNotFoundException extends RuntimeException {

    public MenuNotFoundException(Integer id) {
        super("Could not find Menu - " + id);
    }
}