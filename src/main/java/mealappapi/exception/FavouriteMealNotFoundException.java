package mealappapi.exception;

public class FavouriteMealNotFoundException extends RuntimeException {

    public FavouriteMealNotFoundException(Integer id) {
        super("Could not find FavouriteMeal - " + id);
    }
}