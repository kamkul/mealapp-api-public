package mealappapi.exception;

public class OpenHoursNotFoundException extends RuntimeException {

    public OpenHoursNotFoundException(Integer id) {
        super("Could not find OpenHours - " + id);
    }
}