package mealappapi.exception;

public class VoivodeshipNotFoundException extends RuntimeException {

    public VoivodeshipNotFoundException(String name) {
        super("Could not find Voivodeship - " + name);
    }

    public VoivodeshipNotFoundException(int id) {
        super("Could not find Voivodeship - " + id);
    }
}