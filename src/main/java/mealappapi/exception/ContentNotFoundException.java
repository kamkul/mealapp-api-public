package mealappapi.exception;

public class ContentNotFoundException extends RuntimeException {

    public ContentNotFoundException(Integer id) {
        super("Could not find Content - " + id);
    }
}