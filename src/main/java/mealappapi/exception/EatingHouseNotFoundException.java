package mealappapi.exception;

public class EatingHouseNotFoundException extends RuntimeException {

    public EatingHouseNotFoundException(String name) {
        super("Could not find EatingHouse - " + name);
    }

    public EatingHouseNotFoundException(Integer id) {
        super("Could not find EatingHouse - " + id);
    }
}