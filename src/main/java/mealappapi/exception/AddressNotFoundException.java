package mealappapi.exception;

public class AddressNotFoundException extends RuntimeException {

    public AddressNotFoundException(Integer id) {
        super("Could not find Address - " + id);
    }
}