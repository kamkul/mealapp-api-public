package mealappapi.advice;

import mealappapi.exception.ContentNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
public class ContentNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(ContentNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String contentNotFoundHandler(ContentNotFoundException ex) {
        return ex.getMessage();
    }
}