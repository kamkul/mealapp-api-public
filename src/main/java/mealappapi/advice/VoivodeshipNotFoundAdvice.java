package mealappapi.advice;

import mealappapi.exception.UserNotFoundException;
import mealappapi.exception.VoivodeshipNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class VoivodeshipNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String voivodeshipNotFoundHandler(VoivodeshipNotFoundException ex) {
        return ex.getMessage();
    }
}