package mealappapi.advice;

import mealappapi.exception.FavouriteMealNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
public class FavouriteMealNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(FavouriteMealNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String favouriteMealTagNotFoundHandler(FavouriteMealNotFoundException ex) {
        return ex.getMessage();
    }
}