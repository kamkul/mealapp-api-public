package mealappapi.advice;

import mealappapi.exception.MenuNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
public class MenuNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(MenuNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String menuTagNotFoundHandler(MenuNotFoundException ex) {
        return ex.getMessage();
    }
}