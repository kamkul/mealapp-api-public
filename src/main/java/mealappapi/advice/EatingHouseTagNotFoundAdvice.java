package mealappapi.advice;

import mealappapi.exception.EatingHouseTagNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
public class EatingHouseTagNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(EatingHouseTagNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String eatingHouseTagNotFoundHandler(EatingHouseTagNotFoundException ex) {
        return ex.getMessage();
    }
}