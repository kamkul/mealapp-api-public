package mealappapi.advice;

import mealappapi.exception.OpenHoursNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
public class OpenHoursNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(OpenHoursNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String openHoursTagNotFoundHandler(OpenHoursNotFoundException ex) {
        return ex.getMessage();
    }
}