package mealappapi.advice;

import mealappapi.exception.EatingHouseNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@RestControllerAdvice
public class EatingHouseNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(EatingHouseNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String eatingHouseNotFoundHandler(EatingHouseNotFoundException ex) {
        return ex.getMessage();
    }
}