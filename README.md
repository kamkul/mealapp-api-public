Wersja Gradle
````
6.2.2
````

Uwaga - jeśli nie masz działającego Gradle to potrzebujesz ściągnąć:
```
https://gradle.org/next-steps/?version=6.2.2&format=bin
```
Wypakować w na przykład 
```
C:\Gradle\
```

A następnie dodać zmienną środowiskową
```
C:\Gradle\gradle-6.2.2\bin
```

Następnie zweryfikować czy Gradle działa poprzez wykonanie w cmd
```
gradle -v
```
Jeśli komenda się wykona to gut, pamiętaj aby uruchomić cmd na nowo po dodaniu zmiennej środowiskowej!

Kompilacja
```
gradle wrapper
./gradlew build
mkdir -p build/dependency && (cd build/dependency; jar -xf ../libs/*.jar)
```

Co potrzebujesz, aby odpalić ten projekt?
```
https://www.docker.com/products/docker-desktop
```

Jeśli korzystasz z Windows 10, naciśnij "Download for Windows", następnie zainstaluj.

Uwaga, jeśli korzystasz z VirtualBoxa, przestanie on działać po instalacji Dockera, aby przywrócić działanie VirtualBoxa począwszy od wersji 6, należy wkleić taką linię do cmd uruchomionego jako admin
```
VBoxManage setextradata global "VBoxInternal/NEM/UseRing0Runloop" 0
```

Klonujemy projekt, aby go uruchomić wpisujemy w cmd, pracującym na katalogu głównym projektu
```
docker-compose up --build
```

Nasze api powinno być dostępne pod adresem
```
localhost:8080
```

Gdyby występował taki błąd w IDEA
```
cannot access org.springframework.context.ConfigurableApplicationContext class file
```
Należy usunąć wygenerowany plik .iml

Przygotował: Kamil