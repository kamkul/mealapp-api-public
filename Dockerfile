FROM openjdk:8-jdk-alpine
ARG DEPENDENCY=build/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app

# RUN addgroup -S spring && adduser -S spring -G spring
# USER spring:spring

ENTRYPOINT ["java","-cp","app:app/lib/*","mealappapi.Application"]